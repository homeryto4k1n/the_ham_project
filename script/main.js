$(function() {
  let navbarLinks = $('.navbar-nav-link'),
    tabs = $('.tab');
  navbarLinks.on('click', function(e) {
    e.preventDefault();

    navbarLinks.removeClass('active');
    $(this).addClass('active');
    tabs.removeClass('active');
    let index = navbarLinks.index(this);
    $(tabs[index]).addClass('active');
  });

  $('.navbar-nav-link-filter').click(function(e) {
    e.preventDefault();

    let category = $(this).data('category');
    $('.navbar-nav-item-filter').removeClass('active');
    $(this).parent().addClass('active');

    if (category == 'all') {
      $('.works-gallery-item:not(.hidden)').show();
    } else {
      $('.works-gallery-item').hide();
      $('.works-gallery-item[data-category="' + category + '"]:not(.hidden)').show();
    }
  });

  $('.load-more').click(function(e) {
    e.preventDefault();

    let category = $('.navbar-nav-item-filter.active .navbar-nav-link-filter').data('category');
    if (category == 'all') {
      $('.works-gallery-item.hidden').show();
    } else {
      $('.works-gallery-item[data-category="' + category + '"]').show();
    }
    $(this).hide();
  })

  let carouselItem = $('.container-item'), min = 0, max = (carouselItem[0].offsetWidth * carouselItem.length) - 240,
    animationTime = 500, animationInProgress = false;
  $('.control.right').on('click', function(e) {
    e.preventDefault();

    let current = Math.abs(parseInt($('.carousel-container').css('left')));
    if (current >= max || animationInProgress) {
      return
    }

    animationInProgress = true;
    setTimeout(function() {
      animationInProgress = false
    }, animationTime);
    $('.carousel-container').css('left', '-' + (current + 120) + 'px');
  });

  $('.control.left').on('click', function(e) {
    e.preventDefault();

    let current = Math.abs(parseInt($('.carousel-container').css('left')));
    if (current <= min || animationInProgress) {
      return
    }

    animationInProgress = true;
    setTimeout(function() {
      animationInProgress = false
    }, animationTime);
    $('.carousel-container').css('left', '-' + (current - 120) + 'px');
  });

  $('.container-item').click(function(e) {
    e.preventDefault();

    let name = $(this).data('name'), position = $(this).data('position'), image = $(this).find('.person-image').css('backgroundImage');

    $('.container-item .person-image').removeClass('active');
    $(this).find('.person-image').addClass('active');

    $('.person .person-name').text(name);
    $('.person .person-position').text(position);
    $('.person .person-image').css('backgroundImage', image);
  });
});
